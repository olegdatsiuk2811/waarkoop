# What was changed:
* removed all code connected with UI tests (like WebDriver)
* removed all code connected with Gradle
* updated POM.xml with new versions of dependencies
* added class [BaseSpecification.java](src%2Ftest%2Fjava%2Fstarter%2Fstepdefinitions%2FBaseSpecification.java) for set base request specification
* added class [GeneralStepDefinitions.java](src%2Ftest%2Fjava%2Fstarter%2Fstepdefinitions%2FGeneralStepDefinitions.java) with common step definitions
* refactored [SearchStepDefinitions.java](src%2Ftest%2Fjava%2Fstarter%2Fstepdefinitions%2FSearchStepDefinitions.java)
* in [search_product_feature.feature](src%2Ftest%2Fresources%2Ffeatures%2Fsearch%2Fsearch_product_feature.feature) now we have scenario outline for positive scenario, and simple scenario for negative check 


# How to run tests locally? 
Use `mvn clean test serenity:aggregate`

You can find report in **_target/site/index.html_**

# How to run tests in Gitlab?
* Go to CICD -> Pipelines

![img_1.png](src/test/resources/img/img_1.png)

* Click 'Run pipeline' button

![img_2.png](src/test/resources/img/img_2.png)

* Click 'Run pipeline' button again

![img_3.png](src/test/resources/img/img_3.png)

* Start your tests

![img_4.png](src/test/resources/img/img_4.png)

* Wait until the job is finished and click on pages

![img_5.png](src/test/resources/img/img_5.png)
* Click on 'Browse'

![img_6.png](src/test/resources/img/img_6.png)

* Open folder public/serenity and click on index.html

![img_7.png](src/test/resources/img/img_7.png)

* Then open the link and you will see the report

![img_8.png](src/test/resources/img/img_8.png)
![img_9.png](src/test/resources/img/img_9.png)
