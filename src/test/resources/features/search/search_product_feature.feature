Feature: Search for the product

  Scenario Outline: Search products with valid category
    When he calls endpoint '/search/demo/<endpoint>/'
    Then status code is 200
    And list of products is not empty
    And all following required fields are not null
      | provider     |
      | title        |
      | url          |
      | brand        |
      | price        |
      | unit         |
      | isPromo      |
      | promoDetails |
      | image        |
    And every product contains text '<expected_text_in_title>' in 'title'

    Examples:
      | endpoint | expected_text_in_title |
      | apple    | apple                  |
      | orange   | orange                 |
      | pasta    | pasta                  |
      | cola     | cola                   |


  Scenario: Search products with invalid category
    When he calls endpoint '/search/demo/car/'
    Then status code is 404
    And field 'detail.error' is equals to true
    And field 'detail.message' is equals to 'Not found'
    And field 'detail.requested_item' is equals to 'car'
    And field 'detail.served_by' is equals to 'https://waarkoop.com'