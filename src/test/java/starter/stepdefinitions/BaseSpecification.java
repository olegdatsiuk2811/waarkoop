package starter.stepdefinitions;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;

public class BaseSpecification {

    static {
        RequestSpecification defaultSpec = new RequestSpecBuilder()
                .setBaseUri("https://waarkoop-server.herokuapp.com")
                .setBasePath("/api/v1")
                .addFilter(new RequestLoggingFilter())
                .addFilter(new ResponseLoggingFilter()).build();
        SerenityRest.setDefaultRequestSpecification(defaultSpec);
    }
}