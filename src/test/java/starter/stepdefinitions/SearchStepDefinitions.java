package starter.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.hamcrest.Matchers;

import java.util.List;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions extends BaseSpecification {

    @Then("list of products is not empty")
    public void listOfProductsIsNotEmpty() {
        restAssuredThat(response -> response.body(is(not(emptyArray()))));
    }

    @And("all following required fields are not null")
    public void allFollowingRequiredFieldsAreNotNull(List<String> requiredFields) {
        for (String requiredField : requiredFields) {
            restAssuredThat(response -> response.body(requiredField, Matchers.notNullValue()));
        }
    }

    @Then("every product contains text {string} in {string}")
    public void everyProductHasTitleAccordingToRequest(String expectedTextInTitle, String field) {
        restAssuredThat(response -> response.body(field, Matchers.everyItem(containsString(expectedTextInTitle))));
    }
}