package starter.stepdefinitions;

import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import org.hamcrest.Matchers;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class GeneralStepDefinitions extends BaseSpecification {

    @ParameterType(value = "true|false")
    public Boolean booleanValue(String value) {
        return Boolean.valueOf(value);
    }

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String endpoint) {
        SerenityRest.given().get(endpoint);
    }

    @Then("status code is {int}")
    public void checkThatStatusCodeIs(int expectedStatusCode) {
        restAssuredThat(response -> response.statusCode(expectedStatusCode));
    }

    @Then("field {string} is equals to {booleanValue}")
    public void fieldValueIsEqualsTo(String field, Boolean expectedStingValue) {
        restAssuredThat(response -> response.body(field, Matchers.equalTo(expectedStingValue)));
    }

    @Then("field {string} is equals to {string}")
    public void fieldValueIsEqualsTo(String field, String expectedStingValue) {
        restAssuredThat(response -> response.body(field, Matchers.equalTo(expectedStingValue)));
    }
}